========
Overview
========

What is Thomson Default Wireless KeyGenerator:

Is a simple, lightweight app with a simple UI frontend based on STKEYS (http://www.hakim.ws/st585/KevinDevine/) providing default passwords for Thomson routers. This latest version has a ported version (C to Java) of the keygen algorithm.

How to use it:

The User Interface is pretty simple,just fill in the routers ESSID (only the number and letters after Thomson) and it will instantly generate 
possible keys to access the network.
If you are using linux you will be able to use the button scan wireless networks that invokes the command iwlist scan
and will give you information about available networks.
Theres also a print button to print the possible keys or the info regarding the wireless information.

Requirements:

-This program can run on any platform (Operating System) as long as you have java runtime enviroment installed.

To run the project after extracting the .zip file, go to the extracted folder and double-click the "ThomsonKeyGen.jar" if you have the java runtime enviroment installed (jre 1.6 or later).
If you don't, then go to http://javadl.sun.com/webapps/download/AutoDL?BundleId=20287 , download it and install it.


To run the project from the command line, go to the D-link Default Wireless KeyGen folder and
type the following:

java -jar "ThomsonKeyGen.jar" 

To distribute this project, zip up the main folder (including the lib folder)
and distribute the ZIP file.

=====================================

Author/Developer 
    Nuno Khan
Contributors 
    Matto Esaurito (provided patch for 2011 router compatibility)

=====================================

=====================================

Stkeys support
	http://www.hakim.ws/st585/KevinDevine/
	
=====================================
